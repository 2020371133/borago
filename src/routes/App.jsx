import React from "react";
import "@styles/global.css";
import Layout from "@containers/Layout";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "@pages/Home";
import Login from "@pages/Login";
import Crearcuenta from "@pages/CrearCuenta";
import E404 from "@pages/E404";
import Categorias from "@pages/categorias";
import { CategoriaList } from "../pages/CategoriaList";
import DetallePelicula from "../pages/DetallePelicula";
import MiCuenta from "../pages/MiCuenta";
import FavoritosContext from "../context/favoritosContext";
import useEstadoInicial from "../hooks/useEstadoInicial";

const App = () => {
  const estadoInicial = useEstadoInicial();
  return (
    <FavoritosContext.Provider value={estadoInicial}>
      <BrowserRouter>
        <Layout>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/CrearCuenta" element={<Crearcuenta />} />
            <Route path="/*" element={<E404 />} />
            <Route path="/categorias" element={<Categorias />} />
            <Route path="/categorias/:id" element={<CategoriaList />} />
            <Route path="/detallepelicula/:idP" element={<DetallePelicula />} />
            <Route path="/categorias/:id/detallepelicula/:idP" element={<DetallePelicula />} />
            <Route path="/peliculas" element={<DetallePelicula />} />
            <Route path="/micuenta" element={<MiCuenta />} />
          </Routes>
        </Layout>
      </BrowserRouter>
    </FavoritosContext.Provider>
  );
};

export default App;
