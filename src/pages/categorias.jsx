import React from "react";
import '@styles/Categorias.scss';
import { useNavigate } from "react-router-dom";
const Categorias = () => {
    const navigate = useNavigate();
    return (
        <div className="container text-center categoriasS">
            <br></br>
            <br></br>
            <div className="row">
                <div class="card col-12 col-md-6 col-lg-6 cardS2">
                    <div className="imgcontent" onClick={()=>navigate(`${14}`) }>

                        <h1 className="titleS">FANTASIA</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS3" onClick={()=>navigate(`${16}`)}>
                    <div className="imgcontent">

                        <h1 className="titleS">INFANTILES</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS1" onClick={()=>navigate(`${27}`)}>
                    <div className="imgcontent">
                        <h1 className="titleS">TERROR</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS4" onClick={()=>navigate(`${878}`)}>
                    <div className="imgcontent">

                        <h1 className="titleS">CIENCIA FICCIÓN</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS5" onClick={()=>navigate(`${9648}`)}>
                    <div className="imgcontent">
                        <h1 className="titleS">MISTERIO</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS6" onClick={()=>navigate(`${35}`)}>
                    <div className="imgcontent">
                        <h1 className="titleS">COMEDIA</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS7" onClick={()=>navigate(`${12}`)}>
                    <div className="imgcontent">
                        <h1 className="titleS">AVENTURA</h1>
                    </div>
                </div>
                <div class="card col-12 col-md-6 col-lg-6 cardS8" onClick={()=>navigate(`${28}`)}>
                    <div className="imgcontent">
                        <h1 className="titleS">ACCIÓN</h1>
                    </div>
                </div>
            </div>
            <br></br>
            <br></br>
        </div>
    );
}

export default Categorias;