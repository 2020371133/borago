import React from "react";
import "@styles/CrearCuenta.scss";
import logoCC from "@assets/perfil.png";
import { useNavigate } from "react-router-dom";

const CrearCuenta = () => {

  const navigate = useNavigate()

  return (
    <div className="form">
      <div className="con-crear">
        <div className="form-containerCc">
          <form className="form-loginCc">

            <label htmlFor="email" className="labelCc1">
              Crear Cuenta
            </label>
            <label htmlFor="email" className="labelCc">
              Correo Electronico
            </label>
            <input
              type="email"
              className="inputCc"
              placeholder="tristan.m.p@hotmail.com"
            />
            <label htmlFor="text" className="labelCc">
              Nombre
            </label>
            <input
              type="text"
              className="inputCc"
              placeholder="Nombre completo"
            />
            <label htmlFor="email" className="labelCc">
              Contraseña
            </label>
            <input type="password" className="inputCc" placeholder="********" />
            <label htmlFor="email" className="labelCc">
              Confirmar contraseña
            </label>
            <input type="password" className="inputCc" placeholder="********" />
            <div className="con-btn">
              <a onClick={(() => navigate('/login'))} href="/login">
                <input
                  type="submit"
                  className="boton-principalCc"
                  defaultValue="Suscribirse"
                />
              </a>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
export default CrearCuenta;
