import React from 'react';
import PeliculaList from '@containers/PeliculaList';

const Home = () => {
    return (
        <PeliculaList/>
    );
}

export default Home;
