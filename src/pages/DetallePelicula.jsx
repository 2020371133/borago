import React, { useContext } from "react";
import '@styles/DetallePelicula.scss'
import { useParams } from "react-router-dom";
import { useGetPelicula } from "@hooks/useGetPelicula";
import { TiDeleteOutline } from 'react-icons/ti'
import video from "@assets/video.mp4"
import ReactPlayer from "react-player";
import FavoritosContext from "@context/favoritosContext";
const DetallePelicula = () => {
  const { idP } = useParams();
  const pelicula = useGetPelicula(idP)
  const image = `https://image.tmdb.org/t/p/w500${pelicula.poster_path}`


  const {agregarPelicula} =useContext(FavoritosContext)
  const handleClick = (item) =>{
    agregarPelicula(item)

  }
  return (
    <div className="containerDetallePelicula">
      <br></br>
      <br></br>
      <br></br>
      <div className="row">
        <div className="col-5">
          <div className="cartel">
            <img className="CartelPelicula" src={image} />
          </div>
        </div>
        <div className="col-7">
          <h1>{pelicula.title}</h1>
          <p>{pelicula.overview}</p>
          <p>Calificación</p>
          <p>⚝ ⚝ ⚝ ⚝ ⚝</p>
          <div className="buttonDetallePelicula">
            <button className="btn-ver" onClick={() => handleClick(pelicula)}>Agregar a favoritas</button>
            <button type="button" class="btn btn-primary btn-ver" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
              Ver pelicula
            </button>

            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog modal-fullscreen classMs">
                <div className="modal-content">
                  <div className="modal-header classMs">
                    <h1 className="modal-title fs-5" id="staticBackdropLabel">{pelicula.title}</h1>
                    <button type="button" class="btn-info" data-bs-dismiss="modal" aria-label="Close">X</button>
                  </div>
                  <div className="modal-body classMs">
                    <ReactPlayer
                      width='100%'
                      height='100%'
                      url={video}
                      controls

                    />
                  </div>
                  <div className="modal-footer classMs">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}

export default DetallePelicula;