import React from "react";
import '@styles/MiCuenta.scss';
import user from '@assets/user.png'

const MiCuenta = () => {
    return (
        <div className="MiCuenta">

            <div className="container-micuenta">

                <div className="form-container-micuenta">
                    <div className="titleMiCuenta">
                        <h1>Mi cuenta</h1>
                        <img src={user} alt="logo" className="logoMiCuenta" />
                    </div>
                    <div className="infoMiCuenta">
                        <h2 className="titleInfo">Nombre</h2>
                        <a>Alejandro Gamiño Trejo</a>
                        <h2 className="titleInfo">Correo Electrónico</h2>
                        <a>Alejandro@gmail.com</a>
                        <h2 className="titleInfo">Contraseña</h2>
                        <a>************</a>
                    </div>
                    <div className="con-btn-log">
                        <a href="/login">
                            <button className="boton-principal-micuenta">Editar</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default MiCuenta;