import React, { useRef } from "react";
import '@styles/E404.scss';
import palomitas from '@assets/palomitas.png'
import bob from '@assets/bob.png'
import patricio from '@assets/patricio.png'
import gary from '@assets/gary.png'
import img404 from '@assets/img404.svg'
import axios from "axios";

const E404 = () => {
    return (

        <div className="sala" >



            <div className="brand-logo" >

                <img src="" width="80px" />

            </div >



            <div className="box-404">

                <img className="image-404" src={img404} width="300px" />

            </div>

            <div className="stars">

            </div>

            <a href="" className="btn-go-home" target="_blank" >Home</a >

            <div className="objects" >

                <img className="palomitas" src={palomitas} width="80px" />

                <div className="patricio-gary" >

                    <img className="gary" src={gary} width="100px" />

                    <img className="patricio" src={patricio} />

                </div >

                <div className="box-bob" >

                    <img className="bob" src={bob} width="140px" />

                </div >

            </div >

        </div >
    );
}

export default E404;