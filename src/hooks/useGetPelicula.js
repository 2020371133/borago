import axios from "axios";
import React, { useEffect, useState } from "react";

export const useGetPelicula = (id) => {
  const [pelis, setPelis] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=3154ff2c55fd7004978f22d03af6834e&language=es`
      );
        setPelis(response.data);
    }
    fetchData();
  }, []);
  return pelis;
};
