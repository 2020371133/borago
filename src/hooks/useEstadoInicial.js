import { useState } from "react";

const estadoInicial ={
    pelicula:[]
}

const useEstadoInicial = () => {
    const [state,setState] = useState(estadoInicial);

    const agregarPelicula = (payload)=>{
        state.pelicula.some((item) => item.id === payload.id) ? console.log('ya existe') : setState({
            ...state,
            pelicula:[...state.pelicula,payload]
        })


    }
    const eliminarPelicula= (indexValue)=>{
        console.log(indexValue)
        setState({
            ...state,
            pelicula:
             state.pelicula.filter((producto) => producto.id !== indexValue),
        })
      }
  console.log(state.pelicula)
    return{state, agregarPelicula,eliminarPelicula}
}

export default useEstadoInicial;
