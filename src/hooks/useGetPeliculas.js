import React, { useEffect, useState } from "react";
import axios from "axios";

export const useGetPeliculas = () => {
  const [product, setproduct] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        "https://api.themoviedb.org/3/movie/popular?api_key=3154ff2c55fd7004978f22d03af6834e&language=ES&page=1"
      );
      const data = await response.json();
      setproduct((prevState) => prevState.concat(data.results));
    }

    fetchData();
  }, []);

  // Agregar más peticiones al array
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=3154ff2c55fd7004978f22d03af6834e&language=ES&page=2"
      );
      const data = await response.json();
      setproduct((prevState) => prevState.concat(data.results));
    }

    fetchData();
  }, []);

  // Agregar más peticiones al array
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=3154ff2c55fd7004978f22d03af6834e&language=ES&page=3"
      );
      const data = await response.json();
      setproduct((prevState) => prevState.concat(data.results));
    }

    fetchData();
  }, []);


  return product;
};


export const useGetPopulares = (API) => {
  const [peliculas, setPeliculas] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        "https://api.themoviedb.org/3/movie/popular?api_key=3154ff2c55fd7004978f22d03af6834e&language=ES&page=1"
      );
      const data = await response.json();
      setPeliculas((prevState) => prevState.concat(data.results));
    }

    fetchData();
  }, []);



  return peliculas;
};
