import React from 'react';
import '@styles/MenuMiCuenta.scss'

const MenuMiCuenta = () => {

    return (
        <div className="Menu2">
            <ul>
                <li>
                    <img src="" className="UserMenu2" />
                    <a href="Login">Cerrar sesión</a>
                </li>
                <li>
                    <img src="" className="SignoutMenu2" />
                    <a href="MiCuenta">Perfil</a>
                </li>
            </ul>
        </div>
    );
}

export default MenuMiCuenta;