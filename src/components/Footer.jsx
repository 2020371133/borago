import React from 'react';
import '@styles/Footer.scss';
import logo from '@assets/logo.png'
import { FaFacebookSquare,FaTwitterSquare,FaInstagram, FaYoutube } from "react-icons/fa";

const Footer = () => {

    return (
        <footer className='footer' id='footer'>
            <div class="container-fo">
                <div class="row-fo">
                    <div class="col-md-6">
                        <p class="p-fo">Información de contacto:</p>
                        <ul class="ul-fo">
                            <li class="li-fo">Dirección: Calle Falsa 123, Ciudad Ficticia</li>
                            <li class="li-fo">Teléfono: 555-1234</li>
                            <li class="li-fo">Correo electrónico: info@borage.com</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p class="p-fo">Síguenos en las redes sociales:</p>
                        <div class="social-icons">
                            <a href="https://www.facebook.com/"><FaFacebookSquare size={'30px'}/></a>
                            <a href="https://twitter.com/"><FaTwitterSquare size={'30px'}/></a>
                            <a href="https://www.instagram.com/"><FaInstagram size={'30px'}/></a>
                            <a href="https://www.youtube.com/"><FaYoutube size={'30px'}/></a>
                        </div>
                    </div>
                </div>
                <p class="text-center">© 2023 www.borage.com</p>
            </div>
        </footer>
    );
};

export default Footer;