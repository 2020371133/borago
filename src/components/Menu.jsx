import React from "react";
import '@styles/Menu.scss'

const Menu = () => {
    return (
        <div className="Menu">
            <ul>
                <li>
                    <a href='/'>Inicio</a>
                </li>
                <li>
                    <a href='/'>Peliculas</a>
                </li>
                <li>
                    <a href='/categorias'>Categorias</a>
                </li>
                <li>
                    <img src="" className="UserMenu" />
                    <a href="MiCuenta">Mi cuenta</a>
                </li>
                <li>
                    <img src="" className="SignoutMenu" />
                    <a href="Login">Cerrar sesión</a>
                </li>
            </ul>
        </div>

    );
}

export default Menu;