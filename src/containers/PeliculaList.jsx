import React, { useEffect } from "react";
import '@styles/Home.scss';
import ReactPlayer from "react-player";
import pelicula from '@assets/pelicula.jpg'
import PeliculaItem from "@components/PeliculaItem";
import { useGetPeliculas, useGetPopulares } from "@hooks/useGetPeliculas";
import video from "@assets/video.mp4"
import { useNavigate } from "react-router-dom";

const PeliculaList = () => {
    const populares = useGetPopulares();
    const API = 'https://fakestoreapi.com/products'
    const peliculasL = useGetPeliculas(API)
    const navigate = useNavigate();
    console.log(peliculasL[1])

    useEffect(() => {
        const fila = document.querySelector('.contenedor-carousel');
        const peliculas = document.querySelectorAll('.pelicula');

        const flechaIzquierda = document.getElementById('flecha-izquierda');
        const flechaDerecha = document.getElementById('flecha-derecha');
        const indicadores = document.querySelector('.indicadores');

        // Agregar evento de clic en la flecha derecha
        flechaDerecha.addEventListener('click', () => {
            fila.scrollLeft += fila.offsetWidth;
        });

        // Agregar evento de clic en la flecha izquierda
        flechaIzquierda.addEventListener('click', () => {
            fila.scrollLeft -= fila.offsetWidth;
        });

        const numeroPaginas = Math.ceil(peliculas.length / 5);
        for (let i = 1; i <= numeroPaginas; i++) {
            const indicador = document.createElement('button');
            indicador.innerText = i;
            indicadores.appendChild(indicador);
            indicador.addEventListener('click', () => {
                fila.scrollLeft = (i - 1) * fila.offsetWidth;
                // Cambiar la clase "activo" al indicador seleccionado
                const indicadores = document.querySelectorAll('.indicadores button');
                indicadores.forEach((btn) => {
                    btn.classList.remove('activo');
                });
                indicador.classList.add('activo');
            });
        }

    }, []);
    return (

        <div className="peliculas contenedor">

            <div className="mas-peliculas">
                <div className="peliculas-dos">
                    <ReactPlayer className="video"
                        width='100%'
                        height='100%'
                        url={video}
                        playing
                        controls

                    />
                </div>
            </div>
            <div className="contenedor-titulo">
                <h3>Peliculas Recomendadas</h3>
            </div>
            <div className="contenedor-principal">
                <button role="button" id="flecha-izquierda" class="flecha-izquierda">
                    <i className='icon fas fa-angle-left' />
                </button>
                <div className="contenedor-carousel">
                    <div className="carousel">
                        {populares.map((pelicula, index) => {
                            return <PeliculaItem product={pelicula} key={index} />
                        })}
                    </div>
                </div>
                <button role="button" id="flecha-derecha" class="flecha-derecha">
                    <i className='icon fas fa-angle-right' />
                </button>
            </div>

            <div className="mas-peliculas2">
                <div className="peliculas-dos2">
                    <a href="#"><img src={pelicula} alt="" /></a>
                    <h3 className="titleS">Todas las peliculas.</h3>
                    <h3 className="titleS">Solo en Borage.</h3>
                </div>
            </div>
            <div className="content container">
                <center>
                    <h1 className="titleS">Todas las peliculas</h1>
                </center>
                <div className="row">
                    {peliculasL.map((pelicula) => {
                        return (
                            <div className="card col-6 col-md-2 col-lg-2 cardsSt">
                                <PeliculaItem product={pelicula} key={pelicula.id} />
                            </div>
                        )
                    })}
                </div>
            </div>

        </div>
    );
}

export default PeliculaList;