import React from 'react'
import Header from '@components/Header';
import Footer from '@components/Footer';

const Layout = ({children}) => {
    const url = window.location.href;
    function viewHeader() {
        if (url.includes('Login') || url.includes('crearcuenta') || url.includes('recuperarpass')) {
            window.location.reload
            return false
        } else { return true }
    }

    return (
        <div className='Layout'>
             {viewHeader()? <Header /> : null }
            {children}
            {viewHeader()? <Footer /> : null }
        </div>
    );
}

export default Layout;